//
//  GameScene.h
//  Tower Jumper
//
//  Created by Vaster on 2/19/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>

@interface GameScene : SKScene

@property (nonatomic) NSMutableArray<GKEntity *> *entities;
@property (nonatomic) NSMutableDictionary<NSString*, GKGraph *> *graphs;




@end
