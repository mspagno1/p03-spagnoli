//
//  GameScene.m
//  Tower Jumper
//
//  Created by Vaster on 2/19/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "GameScene.h"
#import "GameSceneMiddle.h"



@implementation GameScene

NSTimeInterval _lastUpdateTime;
SKSpriteNode * background;
SKSpriteNode * player;
SKNode * ground;
SKSpriteNode * blocks[10];

SKAction* jumpLeft;
SKAction* walkLeft;
SKAction* jumpRight;
SKAction* walkRight;




- (void)sceneDidLoad {
    // Setup your scene here
    
    //Set up background
    background = [SKSpriteNode spriteNodeWithImageNamed: @"GroundLvl.png"];
    background.size = self.frame.size;
    [background setPosition: CGPointMake(0,0)];
    background.physicsBody.affectedByGravity = FALSE;
    [self addChild: (background)];
    
    SKPhysicsBody *border = [SKPhysicsBody bodyWithEdgeLoopFromRect: self.frame];
    
    self.physicsBody = border;
    
    //Set up ground
    ground = [SKNode node];
    ground.position = CGPointMake(0, -518);
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, 10)];
    ground.physicsBody.dynamic = NO;
    [self addChild:ground];
    
    
    //Set World gravity
    self.physicsWorld.gravity = CGVectorMake( 0.0, -3.0 );
    
    //Set up player
    SKTexture* TerraGround = [SKTexture textureWithImageNamed:@"TerraFront.gif"];
    
    //Walk action Left
    SKTexture* TerraWalkLeft1 = [SKTexture textureWithImageNamed:@"TerraWalkLeft.gif"];
    TerraWalkLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkLeft2 = [SKTexture textureWithImageNamed:@"TerraWalkLeft2.gif"];
    TerraWalkLeft2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkLeft3 = [SKTexture textureWithImageNamed:@"TerraWalkLeft3.gif"];
    TerraWalkLeft3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkLeft4 = [SKTexture textureWithImageNamed:@"TerraWalkLeft4.gif"];
    TerraWalkLeft4.filteringMode = SKTextureFilteringNearest;
    
    
    walkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWalkLeft1,TerraWalkLeft2,TerraWalkLeft3,TerraWalkLeft4] timePerFrame:0.2]];
    
    
    //Walk action Right
    SKTexture* TerraWalkRight1 = [SKTexture textureWithImageNamed:@"TerraWalkRight1.gif"];
    TerraWalkRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkRight2 = [SKTexture textureWithImageNamed:@"TerraWalkRight2.gif"];
    TerraWalkRight2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkRight3 = [SKTexture textureWithImageNamed:@"TerraWalkRight3.gif"];
    TerraWalkRight3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraWalkRight4 = [SKTexture textureWithImageNamed:@"TerraWalkRight4.gif"];
    TerraWalkRight4.filteringMode = SKTextureFilteringNearest;
    
    
    walkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraWalkRight1,TerraWalkRight2,TerraWalkRight3,TerraWalkRight4] timePerFrame:0.2]];
    
    //Jump action Left
    SKTexture* TerraJumpLeft1 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft.gif"];
    TerraJumpLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraJumpLeft2 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft2.gif"];
    TerraJumpLeft2.filteringMode = SKTextureFilteringNearest;
    
    jumpLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraJumpLeft1, TerraJumpLeft2] timePerFrame:0.2]];
    
    //Jump action Right
    SKTexture* TerraJumpRight1 = [SKTexture textureWithImageNamed:@"TerraJumpRight1.gif"];
    TerraJumpRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerraJumpRight2 = [SKTexture textureWithImageNamed:@"TerraJumpRight2.gif"];
    TerraJumpRight2.filteringMode = SKTextureFilteringNearest;
    
    jumpRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerraJumpRight1, TerraJumpRight2] timePerFrame:0.2]];
    
    
    
    player = [SKSpriteNode spriteNodeWithTexture: TerraGround];
    
    
    [player setPosition: CGPointMake(0,-517)];
    player.size = CGSizeMake(80, 100);
    
    player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:player.texture.size];
    //player.physicsBody = [SKPhysicsBody bodyWithTexture:player.texture size:player.texture.size];
    player.physicsBody.dynamic = YES;
    player.physicsBody.allowsRotation = false;
    
    [self addChild: (player)];
    
    //Make the blocks
    int xPosition[10];
    //int yPosition[10];
    xPosition[0] = -300;
    xPosition[1] = -100;
    xPosition[2] = 200;
    xPosition[3] = 0;
    xPosition[4] = -200;
    xPosition[5] = -0;
    xPosition[6] = -200;
    xPosition[7] = -100;
    xPosition[8] = 100;
    xPosition[9] = 0;
    
    
    for(int i = 0; i < 10 ;i++){
        
        blocks[i] =  [SKSpriteNode node];
        blocks[i].size = CGSizeMake(150.0, 30.0);
        blocks[i].position = CGPointMake(xPosition[i], -518 + (i+1) * 150);
        
        blocks[i].color = [UIColor grayColor];
        //blocks[i].physicsBody.affectedByGravity = FALSE;
        blocks[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:blocks[i].size];
        blocks[i].physicsBody.dynamic = NO;
        
        
        [self addChild: (blocks[i])];
        
    }
    //Directions
    
    
    
}

/*
 -(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
 // Called when a touch begins
 [player.physicsBody applyImpulse:CGVectorMake(20, 20)];
 }
 */

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    //printf("Player loc %f vs touch location %f \n",player.position.x, location.x);
    if(location.y <= player.position.y){
        
        if(location.x > player.position.x && location.y){
            [player.physicsBody applyImpulse:CGVectorMake(5, 0)];
            [player runAction:walkRight];
        }
        else{
            [player.physicsBody applyImpulse:CGVectorMake(-5, 0)];
            [player runAction:walkLeft];
        }
    }
    else{
        if(player.physicsBody.velocity.dy == 0){
           if(location.x > player.position.x && location.y){
            [player.physicsBody applyImpulse:CGVectorMake(5, 30)];
            [player runAction:jumpRight];
           }
           else{
               [player.physicsBody applyImpulse:CGVectorMake(-5, 30)];
               [player runAction:jumpLeft];
           }
        }
           
    }
    
}

-(void) update:(NSTimeInterval)currentTime{
    //printf("Player loc %f vs touch location %f \n",player.position.y, blocks[9].position.y);
    /* Not sure why its not working
    if(player.position.y > 585){
     
        SKTransition *reveal = [SKTransition revealWithDirection:SKTransitionDirectionUp duration:0.2];
        GameSceneMiddle *newGameScene = [[GameSceneMiddle alloc] initWithSize: self.frame.size];
        //  Optionally, insert code to configure the new scene.
        newGameScene.scaleMode = SKSceneScaleModeAspectFill;
        [self.scene.view presentScene: newGameScene transition: reveal];
    
    }
    */
    
}





@end
