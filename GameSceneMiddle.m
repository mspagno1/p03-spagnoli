//
//  GameSceneMiddle.m
//  Tower Jumper
//
//  Created by Vaster on 2/26/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "GameSceneMiddle.h"
#import "GameScene.h"

@implementation GameSceneMiddle

SKSpriteNode * newBackground;
SKNode * bottom;
SKSpriteNode * middleBlocks[10];
SKSpriteNode * playerMiddle;

SKAction* jumpLeftMiddle;
SKAction* walkLeftMiddle;
SKAction* jumpRightMiddle;
SKAction* walkRightMiddle;
//Middle scene of game

- (void)sceneDidLoad {
    // Setup your scene here
    
    //Set up background
    newBackground = [SKSpriteNode spriteNodeWithImageNamed: @"BackWall.png"];
    newBackground.size = self.frame.size;
    [newBackground setPosition: CGPointMake(0,0)];
    newBackground.physicsBody.affectedByGravity = FALSE;
    [self addChild: (newBackground)];
    
    SKPhysicsBody *border = [SKPhysicsBody bodyWithEdgeLoopFromRect: self.frame];
    
    self.physicsBody = border;
    
    //Set up ground
    bottom = [SKNode node];
    bottom.position = CGPointMake(0, -518);
    bottom.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, 10)];
    bottom.physicsBody.dynamic = NO;
    [self addChild:bottom];
    
    //Set up playerMiddleMiddleMiddle
    SKTexture* TerraGround = [SKTexture textureWithImageNamed:@"TerraFront.gif"];
    
    //Walk action Left
    SKTexture* TerrawalkLeftMiddle1 = [SKTexture textureWithImageNamed:@"TerrawalkLeft.gif"];
    TerrawalkLeftMiddle1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerrawalkLeftMiddle2 = [SKTexture textureWithImageNamed:@"TerrawalkLeft2.gif"];
    TerrawalkLeftMiddle2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerrawalkLeftMiddle3 = [SKTexture textureWithImageNamed:@"TerrawalkLeft3.gif"];
    TerrawalkLeftMiddle3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerrawalkLeftMiddle4 = [SKTexture textureWithImageNamed:@"TerrawalkLeft4.gif"];
    TerrawalkLeftMiddle4.filteringMode = SKTextureFilteringNearest;
    
    
    walkLeftMiddle = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerrawalkLeftMiddle1,TerrawalkLeftMiddle2,TerrawalkLeftMiddle3,TerrawalkLeftMiddle4] timePerFrame:0.2]];
    
    
    //Walk action Right
    SKTexture* TerrawalkRightMiddle1 = [SKTexture textureWithImageNamed:@"TerrawalkRight1.gif"];
    TerrawalkRightMiddle1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerrawalkRightMiddle2 = [SKTexture textureWithImageNamed:@"TerrawalkRight2.gif"];
    TerrawalkRightMiddle2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerrawalkRightMiddle3 = [SKTexture textureWithImageNamed:@"TerrawalkRight3.gif"];
    TerrawalkRightMiddle3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerrawalkRightMiddle4 = [SKTexture textureWithImageNamed:@"TerrawalkRight4.gif"];
    TerrawalkRightMiddle4.filteringMode = SKTextureFilteringNearest;
    
    
    walkRightMiddle = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerrawalkRightMiddle1,TerrawalkRightMiddle2,TerrawalkRightMiddle3,TerrawalkRightMiddle4] timePerFrame:0.2]];
    
    //Jump action Left
    SKTexture* TerrajumpLeftMiddle1 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft.gif"];
    TerrajumpLeftMiddle1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerrajumpLeftMiddle2 = [SKTexture textureWithImageNamed:@"TerraVictoryLeft2.gif"];
    TerrajumpLeftMiddle2.filteringMode = SKTextureFilteringNearest;
    
    jumpLeftMiddle = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerrajumpLeftMiddle1, TerrajumpLeftMiddle2] timePerFrame:0.2]];
    
    //Jump action Right
    SKTexture* TerrajumpRightMiddle1 = [SKTexture textureWithImageNamed:@"TerrajumpRight1.gif"];
    TerrajumpRightMiddle1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* TerrajumpRightMiddle2 = [SKTexture textureWithImageNamed:@"TerrajumpRight2.gif"];
    TerrajumpRightMiddle2.filteringMode = SKTextureFilteringNearest;
    
    jumpRightMiddle = [SKAction repeatActionForever:[SKAction animateWithTextures:@[TerrajumpRightMiddle1, TerrajumpRightMiddle2] timePerFrame:0.2]];
    
    
    
    playerMiddle = [SKSpriteNode spriteNodeWithTexture: TerraGround];
    
    
    [playerMiddle setPosition: CGPointMake(0,-517)];
    playerMiddle.size = CGSizeMake(80, 100);
    
    playerMiddle.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:playerMiddle.texture.size];
    //playerMiddleMiddle.physicsBody = [SKPhysicsBody bodyWithTexture:playerMiddle.texture size:playerMiddle.texture.size];
    playerMiddle.physicsBody.dynamic = YES;
    playerMiddle.physicsBody.allowsRotation = false;
    
    [self addChild: (playerMiddle)];

    
    //Make the blocks
    int xPosition[10];
    //int yPosition[10];
    xPosition[0] = -300;
    xPosition[1] = -100;
    xPosition[2] = 200;
    xPosition[3] = 0;
    xPosition[4] = -200;
    xPosition[5] = -0;
    xPosition[6] = -200;
    xPosition[7] = -100;
    xPosition[8] = 100;
    xPosition[9] = 0;
    
    
    for(int i = 0; i < 10 ;i++){
        
        middleBlocks[i] =  [SKSpriteNode node];
        middleBlocks[i].size = CGSizeMake(150.0, 30.0);
        middleBlocks[i].position = CGPointMake(xPosition[i], -518 + (i+1) * 150);
        
        middleBlocks[i].color = [UIColor grayColor];
        //blocks[i].physicsBody.affectedByGravity = FALSE;
        middleBlocks[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:middleBlocks[i].size];
        middleBlocks[i].physicsBody.dynamic = NO;
        
        
        [self addChild: (middleBlocks[i])];
        
    }
    //Directions
    
    
    
}


-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    //printf("playerMiddle loc %f vs touch location %f \n",playerMiddle.position.x, location.x);
    if(location.y <= playerMiddle.position.y){
        
        if(location.x > playerMiddle.position.x && location.y){
            [playerMiddle.physicsBody applyImpulse:CGVectorMake(5, 0)];
            [playerMiddle runAction:walkRightMiddle];
        }
        else{
            [playerMiddle.physicsBody applyImpulse:CGVectorMake(-5, 0)];
            [playerMiddle runAction:walkLeftMiddle];
        }
    }
    else{
        if(playerMiddle.physicsBody.velocity.dy == 0){
            if(location.x > playerMiddle.position.x && location.y){
                [playerMiddle.physicsBody applyImpulse:CGVectorMake(5, 30)];
                [playerMiddle runAction:jumpRightMiddle];
            }
            else{
                [playerMiddle.physicsBody applyImpulse:CGVectorMake(-5, 30)];
                [playerMiddle runAction:jumpLeftMiddle];
            }
        }
        
    }
    
}




@end
